import { currentServer } from './create-websocket-server.js';
import config from './config.js';

function receiveHeartbeat() {
  this.isAlive = true;
  console.log(`< received pong`);
}

function handlePing(ws) {
  if(!ws.isAlive) {
    return ws.terminate();
  }

  ws.isAlive = false;
  ws.ping(() => {
    console.log(`< sending ping`);
  });
}

function ping() {
  currentServer.clients.forEach(ws => handlePing(ws));
}

const heartbeat = {
  init: ws => ws.on('pong', receiveHeartbeat),
  interval: setInterval(() => ping, config.heartbeat.interval),
}

const heartbeatInit = ws => ws.on('pong', receiveHeartBeat);

export { heartbeat };
