import { createWebSocketServer } from './create-websocket-server.js';

function start() {
  return createWebSocketServer();
};

const server = { start };

export { server };
