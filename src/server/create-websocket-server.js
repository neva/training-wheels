import WebSocket from 'ws';
import config from './config.js';
import { initializeWebSocketServer } from './initialize-websocket-server.js';

let wss;

const createWebSocketServer = () => {
  if(!wss) {
    wss = new WebSocket.Server(config.websocket);

    initializeWebSocketServer(wss);
  }

  console.log(`Websocket server running on port ${config.websocket.port}`);
  return wss;
};

export { createWebSocketServer, wss };
