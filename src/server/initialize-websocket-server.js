import { heartbeat } from './heartbeat.js';

const initializeWebSocketServer = wss => {
  console.log(`Init`);

  wss.on('listening', () => console.log(`< actually listening`));
  // wss.on('headers', (headers, req) => console.log(headers, req));
  wss.on('connection', (ws, req) => {
    heartbeat.init(ws);
    console.log(`< client ${req.connection.remoteAddress} connected`);

    ws.on('message', msg => console.log(`< from client "${msg}"`));
  });

  console.log(`< ${wss.readyState}`);
};

export { initializeWebSocketServer };
