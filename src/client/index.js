import WebSocket from 'ws';

let ws;

function start() {
  if(!ws) {
    ws = new WebSocket('http://[::1]:8080/');
  }

  ws.on('open', () => console.log(`> connected`));
  ws.on('message', data => console.log(`> from server "${data}"`));
  ws.on('error', err => console.log(`> error ${error}`, error));
  return ws;
}

const client = { start };

export { client, ws };
