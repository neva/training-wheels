import { client } from './client';
import { server } from './server';

const wss = server.start();
const ws = client.start();

console.log(`:`, wss.address());
